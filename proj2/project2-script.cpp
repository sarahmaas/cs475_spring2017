/* 
 *  Sarah Maas
 *  CS 475 Parallel Programming Project 2
 *  May 1, 2017
 */

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//#define NUMNODES ?
//#define NUMT 1
#define ARRAYSIZE 32768
//#define CHUNKSIZE 1
//#define SCHEDULE static
#define STR(str) #str
#define STRING(str) STR(str)
float Array[ARRAYSIZE];
float Ranf( unsigned int *seedp,  float low, float high );

int main(int argc, char *argv[ ] ) {
    #ifndef _OPENMP
        fprintf( stderr, "OpenMP is not supported here -- sorry.\n" );
        return 1;
    #endif 
 
    unsigned int seed = 0;	// a thread-private variable

    // fill array with random numbers
    for (int i = 0; i < ARRAYSIZE; i++) {
        Array[i] = Ranf( &seed, -1.f, 1.f );        
    }
    omp_set_num_threads( NUMT );
   // fprintf( stderr, "Using %d threads\n", NUMT );
    
    // Do threaded stuff here
    double time0 = omp_get_wtime( );
    #pragma omp parallel for default(none), shared(Array), schedule(SCHEDULE, CHUNKSIZE)
    //int testNumMuled = 0;
    // nested loops doing stuff
    for (int i = 0; i < ARRAYSIZE; i++) {
        float product = 1.;
        for (int j = 0; j <= i; j++) {
            product *= Array[j];
            //testNumMuled++;
            //printf("%d - i: %d, j: %d\n", testNumMuled, i, j);
        }
    }
    // end threaded stuff, so end timer
    double time1 = omp_get_wtime( );


    // count of how many multiplications were done:
    long int numMuled = (long int)ARRAYSIZE * (long int)(ARRAYSIZE+1) / 2;
    //printf("TestNumMuled: %d\n", testNumMuled);
    //printf("NumMuled: %d\n", (int)numMuled);
    fprintf( stderr, "Threads = %2d; ChunkSize = %5d; Scheduling = %s ; MegaMults/sec = %10.2lf\n",
	NUMT, CHUNKSIZE, STRING(SCHEDULE), (double)numMuled/(time1-time0)/1000000. );    
    return 0;
}

float
Ranf( unsigned int *seedp,  float low, float high )
{
	float r = (float) rand_r( seedp );		// 0 - RAND_MAX

	return(   low  +  r * ( high - low ) / (float)RAND_MAX   );
}