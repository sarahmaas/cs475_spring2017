#!/bin/csh
# static vs dynamic
foreach s (static dynamic)
    echo SCHEDULE = $s
    # number of threads:
    foreach t ( 1 2 4 6 8 10 12 14 16)
        echo NUMT = $t
        # number of subdivisions:
        foreach c ( 1 4096 )
            echo CHUNKSIZE = $c
            g++ -DCHUNKSIZE=$c -DNUMT=$t -DSCHEDULE=$s project2-script.cpp -o proj -lm -fopenmp
            ./proj >> output.txt
        end
    end
end