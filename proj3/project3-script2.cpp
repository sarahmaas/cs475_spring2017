/* 
 *  Sarah Maas
 *  CS 475 Parallel Programming Project 3
 *  May 9, 2017
 */

#include <omp.h>
#include <stdlib.h>
#include <stdio.h>

//#define NUMT 8

struct s
{
	float value;
	//:int pad[NUMPAD];
} Array[4];

int main(int argc, char *argv[ ] ) {
    #ifndef _OPENMP
        fprintf( stderr, "OpenMP is not supported here -- sorry.\n" );
        return 1;
    #endif 

	omp_set_num_threads( NUMT );

	const int SomeBigNumber = 100000000;	// keep < 2B

    // Do threaded stuff here
    double time0 = omp_get_wtime( );
	#pragma omp parallel for
	for( int i = 0; i < 4; i++ )
	{
		unsigned int seed = 0;		// automatically private
		float tmp = Array[ i ].value;
		for( unsigned int j = 0; j < SomeBigNumber; j++ )
		{
			tmp = tmp + (float)rand_r( &seed );
		}
		Array[ i ].value = tmp;
	}
    // end threaded stuff, so end timer
    double time1 = omp_get_wtime( );
    
    long int numSums = 4*(long int)SomeBigNumber;
    fprintf( stderr, "Fix 2; Threads = %2d; NUMPAD = %2d; MegaSums/sec = %10.2lf\n",
	NUMT, NUMPAD, (double)numSums/(time1-time0)/1000000. );    
    return 0;
}