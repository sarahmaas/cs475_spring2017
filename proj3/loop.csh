#!/bin/csh
uptime
uptime >>& uptime.txt
# number of threads:
foreach t ( 1 2 4 )
    # echo NUMT = $t
    # number of subdivisions:
    foreach n ( 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 )
        # echo NUMPAD = $n
        g++ -DNUMPAD=$n -DNUMT=$t project3-script1.cpp -o proj -lm -fopenmp
        ./proj >>& output.txt 
    end
end

uptime
uptime >>& uptime.txt
# number of subdivisions:
foreach n ( 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 )
# echo NUMPAD = $n
    # number of threads:
    foreach t ( 1 2 4 )
        # echo NUMT = $t
        g++ -DNUMPAD=$n -DNUMT=$t project3-script1.cpp -o proj -lm -fopenmp
        ./proj >>& output.txt 
    end
end

uptime
uptime >>& uptime.txt
# number of threads:
foreach t ( 1 2 4 )
    # echo NUMT = $t
    # number of subdivisions:
    foreach n ( 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 )
        # echo NUMPAD = $n
        g++ -DNUMPAD=$n -DNUMT=$t project3-script2.cpp -o proj -lm -fopenmp
        ./proj >>& output.txt 
    end
end