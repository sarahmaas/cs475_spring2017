#!/bin/bash
 
#part 1-2 local work group sizes to use: 8 32 64 128 256 512
#part 3 local work group sizes to use: 32 64 128 256
#array sizes to use: 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608
 
make clean
 
rm -rf uptime.txt
touch uptime.txt
rm -rf output.txt
touch output.txt
 
for x in {1..10}; do
    for y in {1..10}; do
        # varied local group sizes:
        echo 'FIRST' $x - $y >> output.txt
        echo 'PORTION;\t NMB;\t LOCAL_SIZE;\t NUM_WORK_GROUPS;\t SPEED' >> output.txt
        for s in 8 32 64 128 256 512; do
            # echo LOCAL_SIZE = $s
            for a in 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608; do
                # echo NMB = $a
                uptime >> uptime.txt 2>&1
                g++ -DLOCAL_SIZE=$s -DNMB=$a -o first first.cpp /scratch/cuda-7.0/lib64/libOpenCL.so -lm -fopenmp
                ./first >> output.txt 2>&1
            done
        done
        echo 'SECOND' $x - $y >> output.txt
        echo 'PORTION;\t NMB;\t LOCAL_SIZE;\t NUM_WORK_GROUPS;\t SPEED' >> output.txt
       
        for s in 8 32 64 128 256 512; do
            # echo LOCAL_SIZE = $s
            for a in 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608; do
                # echo NMB = $a
                uptime >> uptime.txt 2>&1
                g++ -DLOCAL_SIZE=$s -DNMB=$a -o second second.cpp /scratch/cuda-7.0/lib64/libOpenCL.so -lm -fopenmp
                ./second >> output.txt 2>&1
            done
        done
    done
done