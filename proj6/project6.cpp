/* 
 *  Sarah Maas
 *  CS 475 Parallel Programming Project 6
 *  June 2, 2017
 */
/* 
 *  The Array Multiply and the Array Multiply-Add can really be the same program. 
 *  Write one program that creates the 4 arrays. Pass A, B, and C into OpenCL, and return D. 
 *  Then all you have to do between the Multiply and Multiply-Add tests is change one line 
 *  in the .cl file. 
 */

// Make this all work for global work sizes in (at least) the range 1K to 8M, and local work sizes 
// in (at least) the range 8 to 512, or up to the maximum work-group size allowed by your system. 
// How you do this is up to you. Use enough values in those ranges to make good graphs. 

int main() {
    
    
    
    return 0;
}