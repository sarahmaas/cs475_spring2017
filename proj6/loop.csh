#part 1-2 local work group sizes to use: 8 32 64 128 256 512
#part 3 local work group sizes to use: 32 64 128 256
#array sizes to use: 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608

#!/bin/csh
make clean

#rm -rf uptime.txt
echo >> uptime.txt
#rm -rf output.txt
echo >> output.txt


foreach z ( 1 2 3 4 5 6 7 8 9 10 )
    # varied local group sizes:
    echo 'FIRST' $z >> output.txt
    echo 'PORTION;\t NMB;\t LOCAL_SIZE;\t NUM_WORK_GROUPS;\t SPEED' >> output.txt
    foreach s ( 8 32 64 128 256 512 )
        # echo LOCAL_SIZE = $s
        foreach a ( 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608 )
            # echo NMB = $a
            uptime >>& uptime.txt
            g++ -DLOCAL_SIZE=$s -DNMB=$a -o first first.cpp /scratch/cuda-7.0/lib64/libOpenCL.so -lm -fopenmp
            ./first >>& output.txt
        end
    end
    echo 'SECOND' $z >> output.txt
    echo 'PORTION;\t NMB;\t LOCAL_SIZE;\t NUM_WORK_GROUPS;\t SPEED' >> output.txt
    
    foreach s ( 8 32 64 128 256 512 )
        # echo LOCAL_SIZE = $s
        foreach a ( 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608 )
            # echo NMB = $a
            uptime >>& uptime.txt
            g++ -DLOCAL_SIZE=$s -DNMB=$a -o second second.cpp /scratch/cuda-7.0/lib64/libOpenCL.so -lm -fopenmp
            ./second >>& output.txt
        end
    end
end
   
