FIRST
NMB;	 LOCAL_SIZE;	 NUM_WORK_GROUPS;	 SPEED
     512;	   8;	        64;	     0.006 GigaMultsPerSecond
    1024;	   8;	       128;	     0.015 GigaMultsPerSecond
    2048;	   8;	       256;	     0.030 GigaMultsPerSecond
    4096;	   8;	       512;	     0.116 GigaMultsPerSecond
    8192;	   8;	      1024;	     0.120 GigaMultsPerSecond
   16384;	   8;	      2048;	     0.228 GigaMultsPerSecond
   32768;	   8;	      4096;	     0.758 GigaMultsPerSecond
   65536;	   8;	      8192;	     1.182 GigaMultsPerSecond
  131072;	   8;	     16384;	     1.680 GigaMultsPerSecond
  262144;	   8;	     32768;	     1.152 GigaMultsPerSecond
  524288;	   8;	     65536;	     1.667 GigaMultsPerSecond
 1048576;	   8;	    131072;	     1.799 GigaMultsPerSecond
 2097152;	   8;	    262144;	     2.388 GigaMultsPerSecond
 4194304;	   8;	    524288;	     2.574 GigaMultsPerSecond
 8388608;	   8;	   1048576;	     2.808 GigaMultsPerSecond
     512;	  32;	        16;	     0.008 GigaMultsPerSecond
    1024;	  32;	        32;	     0.030 GigaMultsPerSecond
    2048;	  32;	        64;	     0.059 GigaMultsPerSecond
    4096;	  32;	       128;	     0.058 GigaMultsPerSecond
    8192;	  32;	       256;	     0.228 GigaMultsPerSecond
   16384;	  32;	       512;	     0.241 GigaMultsPerSecond
   32768;	  32;	      1024;	     0.929 GigaMultsPerSecond
   65536;	  32;	      2048;	     0.973 GigaMultsPerSecond
  131072;	  32;	      4096;	     1.380 GigaMultsPerSecond
  262144;	  32;	      8192;	     1.491 GigaMultsPerSecond
  524288;	  32;	     16384;	     2.705 GigaMultsPerSecond
 1048576;	  32;	     32768;	     4.286 GigaMultsPerSecond
 2097152;	  32;	     65536;	     5.578 GigaMultsPerSecond
 4194304;	  32;	    131072;	     6.699 GigaMultsPerSecond
 8388608;	  32;	    262144;	     8.467 GigaMultsPerSecond
     512;	  64;	         8;	     0.008 GigaMultsPerSecond
    1024;	  64;	        16;	     0.030 GigaMultsPerSecond
    2048;	  64;	        32;	     0.060 GigaMultsPerSecond
    4096;	  64;	        64;	     0.060 GigaMultsPerSecond
    8192;	  64;	       128;	     0.224 GigaMultsPerSecond
   16384;	  64;	       256;	     0.449 GigaMultsPerSecond
   32768;	  64;	       512;	     0.887 GigaMultsPerSecond
   65536;	  64;	      1024;	     1.610 GigaMultsPerSecond
  131072;	  64;	      2048;	     2.109 GigaMultsPerSecond
  262144;	  64;	      4096;	     1.731 GigaMultsPerSecond
  524288;	  64;	      8192;	     0.099 GigaMultsPerSecond
 1048576;	  64;	     16384;	     4.652 GigaMultsPerSecond
 2097152;	  64;	     32768;	     6.899 GigaMultsPerSecond
 4194304;	  64;	     65536;	     8.585 GigaMultsPerSecond
 8388608;	  64;	    131072;	    12.102 GigaMultsPerSecond
     512;	 128;	         4;	     0.008 GigaMultsPerSecond
    1024;	 128;	         8;	     0.030 GigaMultsPerSecond
    2048;	 128;	        16;	     0.061 GigaMultsPerSecond
    4096;	 128;	        32;	     0.090 GigaMultsPerSecond
    8192;	 128;	        64;	     0.232 GigaMultsPerSecond
   16384;	 128;	       128;	     0.432 GigaMultsPerSecond
   32768;	 128;	       256;	     0.899 GigaMultsPerSecond
   65536;	 128;	       512;	     1.919 GigaMultsPerSecond
  131072;	 128;	      1024;	     3.201 GigaMultsPerSecond
  262144;	 128;	      2048;	     0.978 GigaMultsPerSecond
  524288;	 128;	      4096;	     3.077 GigaMultsPerSecond
 1048576;	 128;	      8192;	     5.200 GigaMultsPerSecond
 2097152;	 128;	     16384;	     7.502 GigaMultsPerSecond
 4194304;	 128;	     32768;	     9.060 GigaMultsPerSecond
 8388608;	 128;	     65536;	    12.019 GigaMultsPerSecond
     512;	 256;	         2;	     0.015 GigaMultsPerSecond
    1024;	 256;	         4;	     0.015 GigaMultsPerSecond
    2048;	 256;	         8;	     0.026 GigaMultsPerSecond
    4096;	 256;	        16;	     0.060 GigaMultsPerSecond
    8192;	 256;	        32;	     0.176 GigaMultsPerSecond
   16384;	 256;	        64;	     0.241 GigaMultsPerSecond
   32768;	 256;	       128;	     0.500 GigaMultsPerSecond
   65536;	 256;	       256;	     0.853 GigaMultsPerSecond
  131072;	 256;	       512;	     3.393 GigaMultsPerSecond
  262144;	 256;	      1024;	     1.722 GigaMultsPerSecond
  524288;	 256;	      2048;	     1.998 GigaMultsPerSecond
 1048576;	 256;	      4096;	     4.972 GigaMultsPerSecond
 2097152;	 256;	      8192;	     7.323 GigaMultsPerSecond
 4194304;	 256;	     16384;	     8.880 GigaMultsPerSecond
 8388608;	 256;	     32768;	    12.984 GigaMultsPerSecond
     512;	 512;	         1;	     0.014 GigaMultsPerSecond
    1024;	 512;	         2;	     0.015 GigaMultsPerSecond
    2048;	 512;	         4;	     0.057 GigaMultsPerSecond
    4096;	 512;	         8;	     0.113 GigaMultsPerSecond
    8192;	 512;	        16;	     0.233 GigaMultsPerSecond
   16384;	 512;	        32;	     0.475 GigaMultsPerSecond
   32768;	 512;	        64;	     0.933 GigaMultsPerSecond
   65536;	 512;	       128;	     0.994 GigaMultsPerSecond
  131072;	 512;	       256;	     1.902 GigaMultsPerSecond
  262144;	 512;	       512;	     1.101 GigaMultsPerSecond
  524288;	 512;	      1024;	     3.135 GigaMultsPerSecond
 1048576;	 512;	      2048;	     4.603 GigaMultsPerSecond
 2097152;	 512;	      4096;	     5.240 GigaMultsPerSecond
 4194304;	 512;	      8192;	     9.008 GigaMultsPerSecond
 8388608;	 512;	     16384;	    12.685 GigaMultsPerSecond
SECOND
NMB;	 LOCAL_SIZE;	 NUM_WORK_GROUPS;	 SPEED
     512;	   8;	        64;	     0.007 GigaMultsPerSecond
    1024;	   8;	       128;	     0.015 GigaMultsPerSecond
    2048;	   8;	       256;	     0.060 GigaMultsPerSecond
    4096;	   8;	       512;	     0.113 GigaMultsPerSecond
    8192;	   8;	      1024;	     0.228 GigaMultsPerSecond
   16384;	   8;	      2048;	     0.414 GigaMultsPerSecond
   32768;	   8;	      4096;	     0.751 GigaMultsPerSecond
   65536;	   8;	      8192;	     0.966 GigaMultsPerSecond
  131072;	   8;	     16384;	     1.280 GigaMultsPerSecond
  262144;	   8;	     32768;	     1.095 GigaMultsPerSecond
  524288;	   8;	     65536;	     1.589 GigaMultsPerSecond
 1048576;	   8;	    131072;	     2.062 GigaMultsPerSecond
 2097152;	   8;	    262144;	     2.278 GigaMultsPerSecond
 4194304;	   8;	    524288;	     2.482 GigaMultsPerSecond
 8388608;	   8;	   1048576;	     2.734 GigaMultsPerSecond
     512;	  32;	        16;	     0.015 GigaMultsPerSecond
    1024;	  32;	        32;	     0.029 GigaMultsPerSecond
    2048;	  32;	        64;	     0.060 GigaMultsPerSecond
    4096;	  32;	       128;	     0.055 GigaMultsPerSecond
    8192;	  32;	       256;	     0.110 GigaMultsPerSecond
   16384;	  32;	       512;	     0.242 GigaMultsPerSecond
   32768;	  32;	      1024;	     0.704 GigaMultsPerSecond
   65536;	  32;	      2048;	     1.528 GigaMultsPerSecond
  131072;	  32;	      4096;	     1.896 GigaMultsPerSecond
  262144;	  32;	      8192;	     0.969 GigaMultsPerSecond
  524288;	  32;	     16384;	     0.094 GigaMultsPerSecond
 1048576;	  32;	     32768;	     0.222 GigaMultsPerSecond
 2097152;	  32;	     65536;	     5.320 GigaMultsPerSecond
 4194304;	  32;	    131072;	     0.095 GigaMultsPerSecond
 8388608;	  32;	    262144;	     7.374 GigaMultsPerSecond
     512;	  64;	         8;	     0.015 GigaMultsPerSecond
    1024;	  64;	        16;	     0.028 GigaMultsPerSecond
    2048;	  64;	        32;	     0.046 GigaMultsPerSecond
    4096;	  64;	        64;	     0.108 GigaMultsPerSecond
    8192;	  64;	       128;	     0.119 GigaMultsPerSecond
   16384;	  64;	       256;	     0.250 GigaMultsPerSecond
   32768;	  64;	       512;	     0.913 GigaMultsPerSecond
   65536;	  64;	      1024;	     0.984 GigaMultsPerSecond
  131072;	  64;	      2048;	     1.787 GigaMultsPerSecond
  262144;	  64;	      4096;	     1.672 GigaMultsPerSecond
  524288;	  64;	      8192;	     2.785 GigaMultsPerSecond
 1048576;	  64;	     16384;	     4.089 GigaMultsPerSecond
 2097152;	  64;	     32768;	     6.028 GigaMultsPerSecond
 4194304;	  64;	     65536;	     6.706 GigaMultsPerSecond
 8388608;	  64;	    131072;	     9.895 GigaMultsPerSecond
     512;	 128;	         4;	     0.006 GigaMultsPerSecond
    1024;	 128;	         8;	     0.025 GigaMultsPerSecond
    2048;	 128;	        16;	     0.047 GigaMultsPerSecond
    4096;	 128;	        32;	     0.118 GigaMultsPerSecond
    8192;	 128;	        64;	     0.201 GigaMultsPerSecond
   16384;	 128;	       128;	     0.426 GigaMultsPerSecond
   32768;	 128;	       256;	     0.687 GigaMultsPerSecond
   65536;	 128;	       512;	     0.446 GigaMultsPerSecond
  131072;	 128;	      1024;	     1.888 GigaMultsPerSecond
  262144;	 128;	      2048;	     1.523 GigaMultsPerSecond
  524288;	 128;	      4096;	     2.962 GigaMultsPerSecond
 1048576;	 128;	      8192;	     4.031 GigaMultsPerSecond
 2097152;	 128;	     16384;	     5.372 GigaMultsPerSecond
 4194304;	 128;	     32768;	     7.778 GigaMultsPerSecond
 8388608;	 128;	     65536;	    10.275 GigaMultsPerSecond
     512;	 256;	         2;	     0.009 GigaMultsPerSecond
    1024;	 256;	         4;	     0.029 GigaMultsPerSecond
    2048;	 256;	         8;	     0.043 GigaMultsPerSecond
    4096;	 256;	        16;	     0.060 GigaMultsPerSecond
    8192;	 256;	        32;	     0.222 GigaMultsPerSecond
   16384;	 256;	        64;	     0.482 GigaMultsPerSecond
   32768;	 256;	       128;	     0.818 GigaMultsPerSecond
   65536;	 256;	       256;	     1.778 GigaMultsPerSecond
  131072;	 256;	       512;	     2.033 GigaMultsPerSecond
  262144;	 256;	      1024;	     1.570 GigaMultsPerSecond
  524288;	 256;	      2048;	     2.632 GigaMultsPerSecond
 1048576;	 256;	      4096;	     4.628 GigaMultsPerSecond
 2097152;	 256;	      8192;	     5.692 GigaMultsPerSecond
 4194304;	 256;	     16384;	     7.351 GigaMultsPerSecond
 8388608;	 256;	     32768;	    10.597 GigaMultsPerSecond
     512;	 512;	         1;	     0.014 GigaMultsPerSecond
    1024;	 512;	         2;	     0.016 GigaMultsPerSecond
    2048;	 512;	         4;	     0.056 GigaMultsPerSecond
    4096;	 512;	         8;	     0.114 GigaMultsPerSecond
    8192;	 512;	        16;	     0.118 GigaMultsPerSecond
   16384;	 512;	        32;	     0.480 GigaMultsPerSecond
   32768;	 512;	        64;	     0.489 GigaMultsPerSecond
   65536;	 512;	       128;	     1.853 GigaMultsPerSecond
  131072;	 512;	       256;	     3.202 GigaMultsPerSecond
  262144;	 512;	       512;	     1.125 GigaMultsPerSecond
  524288;	 512;	      1024;	     3.026 GigaMultsPerSecond
 1048576;	 512;	      2048;	     4.878 GigaMultsPerSecond
 2097152;	 512;	      4096;	     5.052 GigaMultsPerSecond
 4194304;	 512;	      8192;	     7.035 GigaMultsPerSecond
 8388608;	 512;	     16384;	    10.763 GigaMultsPerSecond
