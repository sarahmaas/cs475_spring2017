/* 
 *  Sarah Maas
 *  CS 475 Parallel Programming Project 5
 *  May 23, 2017
 */

#include "simd.p5.h"

#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>



//#define ARRAYSIZE       100	// you decide
#define NUMTRIES        10	// you decide

float A[ARRAYSIZE];
float B[ARRAYSIZE];
float C[ARRAYSIZE];

// void	SimdMul(    float *, float *,  float *, int );
// float	SimdMulSum( float *, float *, int );
int main(){
    #ifndef _OPENMP
        fprintf( stderr, "OpenMP is not supported here -- sorry.\n" );
        return 1;
    #endif 

    // initialize arrays to stop compiler optimization
    
    double maxMegaMults = 0.;
    double sumMegaMults = 0.; 
    double avgMegaMults = 0.;


    /* Use the supplied SIMD SSE code to run an array multiplication 
       timing experiment. Run the same experiment a second time using 
       your own C/C++ array multiplication code. */
       
   // SIMD array multiplication
    maxMegaMults = 0.;
    sumMegaMults = 0.; 
    for( int t = 0; t < NUMTRIES; t++ )
    {
        double time0 = omp_get_wtime( );

        SimdMul(A, B, C, ARRAYSIZE);

        double time1 = omp_get_wtime( );
        double megaMults = (double)ARRAYSIZE/(time1-time0)/1000000.;
        sumMegaMults += megaMults;
        if( megaMults > maxMegaMults )
                maxMegaMults = megaMults;
    }
    avgMegaMults = sumMegaMults/(double)NUMTRIES;
    //printf( "     Peak Performance = %8.2lf MegaMults/Sec\n", maxMegaMults );
    //printf( "  Average Performance = %8.2lf MegaMults/Sec\n", avgMegaMults );
    fprintf( stderr, "SIMD Array Mult;             NUMTRIES = %2d; ARRAYSIZE = %2d; Peak MegaMults/Sec = %10.2lf; Average MegaMults/Sec = %10.2lf\n",
	NUMTRIES, ARRAYSIZE, maxMegaMults, avgMegaMults );   
   // own array multiplication
    maxMegaMults = 0.;
    sumMegaMults = 0.; 
    for( int t = 0; t < NUMTRIES; t++ )
    {
        double time0 = omp_get_wtime( );
        for( int i = 0; i < ARRAYSIZE; i++ )
        {
                C[i] = A[i] * B[i];
        }

        double time1 = omp_get_wtime( );
        double megaMults = (double)ARRAYSIZE/(time1-time0)/1000000.;
        sumMegaMults += megaMults;
        if( megaMults > maxMegaMults )
                maxMegaMults = megaMults;
    }
    avgMegaMults = sumMegaMults/(double)NUMTRIES;
    //printf( "     Peak Performance = %8.2lf MegaMults/Sec\n", maxMegaMults );
    //printf( "  Average Performance = %8.2lf MegaMults/Sec\n", avgMegaMults );
    fprintf( stderr, "C++ Array Mult;              NUMTRIES = %2d; ARRAYSIZE = %2d; Peak MegaMults/Sec = %10.2lf; Average MegaMults/Sec = %10.2lf\n",
	NUMTRIES, ARRAYSIZE, maxMegaMults, avgMegaMults );  
     
    /* Use the supplied SIMD SSE code to run an array multiplication +
       reduction timing experiment. Run the same experiment a second time 
       using your own C/C++ array multiplication + reduction code. */
       
    // SIMD array multiplication + reduction
    maxMegaMults = 0.;
    sumMegaMults = 0.; 
    for( int t = 0; t < NUMTRIES; t++ )
    {
        double time0 = omp_get_wtime( );
        
        SimdMulSum(A, B, ARRAYSIZE);
    
        double time1 = omp_get_wtime( );
        double megaMults = (double)ARRAYSIZE/(time1-time0)/1000000.;
        sumMegaMults += megaMults;
        if( megaMults > maxMegaMults )
            maxMegaMults = megaMults;
    }
    avgMegaMults = sumMegaMults/(double)NUMTRIES;
    //printf( "     Peak Performance = %8.2lf MegaMults/Sec\n", maxMegaMults );
    //printf( "  Average Performance = %8.2lf MegaMults/Sec\n", avgMegaMults );
    fprintf( stderr, "SIMD Array Mult + Reduction; NUMTRIES = %2d; ARRAYSIZE = %2d; Peak MegaMults/Sec = %10.2lf; Average MegaMults/Sec = %10.2lf\n",
	NUMTRIES, ARRAYSIZE, maxMegaMults, avgMegaMults );     
    // own array multiplication + reduction
    maxMegaMults = 0.;
    sumMegaMults = 0.; 
    double sum  = 0.;
    for( int t = 0; t < NUMTRIES; t++ )
    {
            double time0 = omp_get_wtime( );

            for( int i = 0; i < ARRAYSIZE; i++ )
            {
                    sum += A[i] * B[i];
            }

            double time1 = omp_get_wtime( );
            double megaMults = (double)ARRAYSIZE/(time1-time0)/1000000.;
            sumMegaMults += megaMults;
            if( megaMults > maxMegaMults )
                    maxMegaMults = megaMults;
    }
    avgMegaMults = sumMegaMults/(double)NUMTRIES;
    //printf( "     Peak Performance = %8.2lf MegaMults/Sec\n", maxMegaMults );
    //printf( "  Average Performance = %8.2lf MegaMults/Sec\n", avgMegaMults );
    fprintf( stderr, "C++ Array Mult + Reduction;  NUMTRIES = %2d; ARRAYSIZE = %2d; Peak MegaMults/Sec = %10.2lf; Average MegaMults/Sec = %10.2lf\n",
	NUMTRIES, ARRAYSIZE, maxMegaMults, avgMegaMults );       
    return 0;
}