#!/bin/csh
make clean
make
# varied array sizes:
foreach s ( 1000 2000 4000 8000 16000 32000 64000 128000 256000 512000 1024000 2048000 4096000 8192000 16384000 32000000 )
    # echo ARRAYSIZE = $s
    g++ -DARRAYSIZE=$s -o arraymult arraymult.cpp simd.p5.o -lm -fopenmp
    ./arraymult
end
