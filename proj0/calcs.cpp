#include <math.h>
#include <iostream>
using std::cout;
using std::endl;

float speedup(float oneP, float parallelP) {
    return parallelP/oneP;
}

float fp(float speedup) {
    return (4./3.)*( 1. - (1./speedup));
}

int main() {
    float s = 0.0;
    float Fp = 0.0;
    float thread1 = 0.0;
    float thread4 = 0.0;    
    
    thread1 = 3315.51;
    thread4 = 6886.14;
    cout << "Speedup and Fp from cloud 9" << endl;
    s = speedup(thread1, thread4);
    cout << "           Speedup (S) = " << s << endl;
    cout << "Parallel Fraction (Fp) = " << fp(s) << endl;
     thread1 = 1730.78;
    thread4 = 4916.55;   
        cout << "Speedup and Fp from flip" << endl;
    s = speedup(thread1, thread4);
    cout << "           Speedup (S) = " << s << endl;
    cout << "Parallel Fraction (Fp) = " << fp(s) << endl;
}