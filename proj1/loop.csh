#!/bin/csh
# number of threads:
foreach t ( 1 2 4 6 8 )
    echo NUMT = $t
    # number of subdivisions:
    foreach s ( 2 4 8 16 32 64 128 256 512 1024 2048 3072 4096 6144 8192 10240 )
        echo NUMNODES = $s
        g++ -DNUMNODES=$s -DNUMT=$t project1-script.cpp -o proj -lm -fopenmp
        ./proj
    end
end