/* MAIN */
    /* set up global variables */
    
    /* calculate temperature and precipitation */
    
    /* spawn threads */
    
        // Watcher
        
        // Grain
        
        // GrainDeer
    
        // MyAgent

/* Watcher */

    /******* DoneComputing barrier *******/
    
    /******* DoneAssigning barrier *******/

    // Print results and increment month.
    // Calculate new temperature and precipitation.
    
    /******* DonePrinting barrier ********/
    
/* Grain */
    // Compute into temp variables.
    
    /******* DoneComputing barrier *******/
    
    // Copy into global variables.
    
    /******* DoneAssigning barrier *******/
    
    /******* DonePrinting barrier ********/
    

    
/* GrainDeer */
    // Compute into temp variables.
    
    /******* DoneComputing barrier *******/
    
    // Copy into global variables.
    
    /******* DoneAssigning barrier *******/
    
    /******* DonePrinting barrier ********/
    
/* MyAgent */
    // Compute into temp variables.
    
    /******* DoneComputing barrier *******/
    
    // Copy into global variables.
    
    /******* DoneAssigning barrier *******/
    
    /******* DonePrinting barrier ********/