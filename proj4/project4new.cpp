/* 
 *  Sarah Maas
 *  CS 475 Parallel Programming Project 4
 *  May 16, 2017
 */

#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/* global variables */
unsigned int seed = 0;  
int	    NowYear;		// 2017 - 2022
int	    NowMonth;		// 0 - 11

float	NowPrecip;		// inches of rain per month
float	NowTemp;		// temperature this month
float	NowHeight;		// grain height in inches
int	    NowNumDeer;		// number of deer in the current population
int     NowNumMyAgent;  // number of predators

const float GRAIN_GROWS_PER_MONTH =		8.0;
const float ONE_DEER_EATS_PER_MONTH =	0.5;

const float AVG_PRECIP_PER_MONTH =		6.0;	// average
const float AMP_PRECIP_PER_MONTH =		6.0;	// plus or minus
const float RANDOM_PRECIP =			    2.0;	// plus or minus noise

const float AVG_TEMP =				    50.0;	// average
const float AMP_TEMP =				    20.0;	// plus or minus
const float RANDOM_TEMP =			    10.0;	// plus or minus noise

const float MIDTEMP =				    40.0;
const float MIDPRECIP =				    10.0;

/* functions */
void Grain();
void GrainDeer();
void Watcher();
void MyAgent();

float Ranf( unsigned int *seedp,  float low, float high );
int Ranf( unsigned int *seedp, int ilow, int ihigh );
float SQR( float x );

/* MAIN */
int main()
{
        /* set up global variables */
     // starting date and time:
    NowMonth =    0;
    NowYear  = 2017;
    
    // starting state:
    NowNumDeer = 1;
    NowHeight =  1.;   
    NowNumMyAgent = 2;
    
    /* calculate temperature and precipitation */
    float ang = (  30.*(float)NowMonth + 15.  ) * ( M_PI / 180. );
    
    float temp = AVG_TEMP - AMP_TEMP * cos( ang );
    unsigned int seed = 0;
    NowTemp = temp + Ranf( &seed, -RANDOM_TEMP, RANDOM_TEMP );
    
    float precip = AVG_PRECIP_PER_MONTH + AMP_PRECIP_PER_MONTH * sin( ang );
    NowPrecip = precip + Ranf( &seed,  -RANDOM_PRECIP, RANDOM_PRECIP );
    if( NowPrecip < 0. )
    NowPrecip = 0.;   
    
    /* spawn threads */
    omp_set_num_threads( 4 );	// same as # of sections
    #pragma omp parallel sections
    {
    	#pragma omp section
    	{
    		GrainDeer( );
    	}
    
    	#pragma omp section
    	{
    		Grain( );
    	}
    
    	#pragma omp section
    	{
    		Watcher( );
    	}
    
    	#pragma omp section
    	{
    		MyAgent( );	// your own
    	}
    }       

    return 0;
}

/* Watcher */
void Watcher()
{
    while( NowYear < 2023 )
    {
        /******* DoneComputing barrier *******/
         #pragma omp barrier
       
        /******* DoneAssigning barrier *******/
        #pragma omp barrier
    
        // Print results and increment month.
 
 /***TODO PRINT RESULTS */       
 
        fprintf( stderr, "NowYear = %2d, NowMonth = %2d, Temperature = %10.2lf, Precipitation = %10.2lf, GrainDeer = %2d, GrainHeight = %10.2lf, Wolves = %2d\n", NowYear, NowMonth, NowTemp, NowPrecip, NowNumDeer, NowHeight, NowNumMyAgent); //TODO comment out

        // temperature, precipitation, number of graindeer, height of the grain, and your own-choice quantity as a function of month number
        NowMonth++;
        // NowMonth %= 12; // not how it should be for graphing
        if (NowMonth % 12 == 0)
        {
            NowYear++;
        }
        

        // Calculate new temperature and precipitation. 
        // The temperature and precipitation are a function of the particular month:
        float ang = (  30.*(float)NowMonth + 15.  ) * ( M_PI / 180. );
    
        float temp = AVG_TEMP - AMP_TEMP * cos( ang );
        //unsigned int seed = 0;
        NowTemp = temp + Ranf( &seed, -RANDOM_TEMP, RANDOM_TEMP );
        
        float precip = AVG_PRECIP_PER_MONTH + AMP_PRECIP_PER_MONTH * sin( ang );
        NowPrecip = precip + Ranf( &seed,  -RANDOM_PRECIP, RANDOM_PRECIP );
        if( NowPrecip < 0. )
        	NowPrecip = 0.;
        	
        /******* DonePrinting barrier ********/    
        #pragma omp barrier

    }
}
    
/* Grain */
void Grain()
{
    while( NowYear < 2023 )
    {
        // Compute into temp variables.
        // compute a temporary next-value for this quantity
	    // based on the current state of the simulation:

        float tempFactor = exp(   -SQR(  ( NowTemp - MIDTEMP ) / 10.  )   );
        
        float precipFactor = exp(   -SQR(  ( NowPrecip - MIDPRECIP ) / 10.  )   );

        float tempHeight = NowHeight;
        tempHeight += tempFactor * precipFactor * GRAIN_GROWS_PER_MONTH;
        tempHeight -= (float)NowNumDeer * ONE_DEER_EATS_PER_MONTH;
        
        // height cannot be less than 0
        if (tempHeight < 0)
        {
            tempHeight = 0;
        }
        
        /******* DoneComputing barrier *******/
        #pragma omp barrier
        
        // Copy into global variables.
        NowHeight = tempHeight;
        
        /******* DoneAssigning barrier *******/
        #pragma omp barrier
        
        /******* DonePrinting barrier ********/
        #pragma omp barrier      
    }
}
  

    
/* GrainDeer */
void GrainDeer()
{
    while( NowYear < 2023 )
    {
        // Compute into temp variables.
        // compute a temporary next-value for this quantity
	    // based on the current state of the simulation:
        int tempNumDeer = NowNumDeer;
        /* The Carrying Capacity of the graindeer is the number of inches of height of the grain. 
           If the number of graindeer exceeds this value at the end of a month, decrease the 
           number of graindeer by one. If the number of graindeer is less than this value at the 
           end of a month, increase the number of graindeer by one. 
        */
        if (tempNumDeer > NowHeight)
        {
            tempNumDeer--;
        }
        else if (tempNumDeer < NowHeight)
        {
            tempNumDeer++;
        }
        if (NowNumMyAgent > 1)
        {
            tempNumDeer--; // hunted
        }
        if (tempNumDeer < 0) 
        {
            tempNumDeer = 0;
        }
        /******* DoneComputing barrier *******/
        #pragma omp barrier
        
        // Copy into global variables.
        NowNumDeer = tempNumDeer;
        
        /******* DoneAssigning barrier *******/
        #pragma omp barrier
        
        /******* DonePrinting barrier ********/
        #pragma omp barrier       
    }
}
  
    
/* MyAgent */
/* if it's below 20 degrees or over 10 inches of precipitation, they're able to sneak up on the deer */
void MyAgent()
{
    while( NowYear < 2023 )
    {
        // Compute into temp variables.
        // compute a temporary next-value for this quantity
	    // based on the current state of the simulation:
	    int x = Ranf( &seed, 0, 10 );
	   // printf("x = %2d\n", x);
        int tmpNumMyAgent = NowNumMyAgent;
        if (x % 2 == 0) {
            tmpNumMyAgent++;
        }
        else {
            tmpNumMyAgent--;
        }
        if (tmpNumMyAgent > NowNumDeer)
        {
            tmpNumMyAgent--; //not enough food!
        }
        if (tmpNumMyAgent < 0)
        {
            tmpNumMyAgent = 0;
        }
        /******* DoneComputing barrier *******/
        #pragma omp barrier
        
        // Copy into global variables.
        NowNumMyAgent = tmpNumMyAgent;
        /******* DoneAssigning barrier *******/
        #pragma omp barrier
        
        /******* DonePrinting barrier ********/
        #pragma omp barrier
    }
}

/* Use as float x = Ranf( &seed, -1.f, 1.f ); */
float Ranf( unsigned int *seedp,  float low, float high )
{
        float r = (float) rand_r( seedp );              // 0 - RAND_MAX

        return(   low  +  r * ( high - low ) / (float)RAND_MAX   );
}


int Ranf( unsigned int *seedp, int ilow, int ihigh )
{
        float low = (float)ilow;
        float high = (float)ihigh + 0.9999f;

        return (int)(  Ranf(seedp, low,high) );
}  

float SQR( float x )
{
        return x*x;
}